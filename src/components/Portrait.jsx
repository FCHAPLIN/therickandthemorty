import React from "react";

export default function Portrait(props) {
  // On récupère les props "character" de notre composant Column
  const {name, image, status, species} = props.character;
  // On affiche les données de chaque personnage
  return (
    <div className="portrait">
      <h3>{name}</h3>
      <img src={image} alt={name} />
      <p className="text">
        {species} - {status}
      </p>
    </div>
  );
}
