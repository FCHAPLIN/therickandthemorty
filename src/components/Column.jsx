import React, { useEffect, useState } from "react";
import Portrait from "./Portrait";

export default function Column(props) {
  // On récupère les props "api" et "title" de notre composant App
  const { api, title } = props;
  // On crée un state "characterList" qui va contenir les données de l'api
  const [characterList, setCharacterList] = useState([]);
  
  // On utilise useEffect pour faire une requete sur l'api
  // La fonction dans useEffect est exécutée à chaque fois que le composant est monté dans le DOM
  // Une seule fois dans ce cas
  useEffect(function () {

    // On utilise fetch pour faire une requete sur l'api
    fetch(api)
      // Fetch renvoie une promesse, on utilise then pour récupérer la réponse
      .then(function (response) {
        // On utilise json() pour convertir la réponse en objet JS
        // json() renvoie une promesse, on utilise then pour récupérer les données
        return response.json();
      })
      // On utilise then pour récupérer les données
      .then(function (data) {
        console.log("result", data.results);
        // On met à jour le state "characterList" avec les données de l'api
        setCharacterList(data.results);
      });
  }, []);

  return (
    <div className="column">
      <h2>{title}</h2>
      {
      // On va mapper les objets de characterList pour créer un composant Portrait pour chaque personnage
      characterList.map((character) => (
        <Portrait key={character.id} character={character} />
      ))}
    </div>
  );
}
