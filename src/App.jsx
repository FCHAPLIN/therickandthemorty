import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import Column from "./components/Column";

function App() {
  
  // Rick and Morty API 
  // Cette URL est une route qui permet de faire une requete 
  // sur les données de l'api "personnages" de Rick et Morty
  const charactersApi = "https://rickandmortyapi.com/api/character";

  return (
    <>
      <h1>TRATM</h1>
      <p>The Rick And The Morty</p>
      <div className="columns-container">
      {/* On passe la props "api" à notre composant Column*/}
        <Column title={"Characters"} api={charactersApi} />
      </div>
    </>
  );
}

export default App;
